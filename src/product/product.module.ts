import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { casePalindromo } from 'src/application/casePalindromo';
import { caseFilter } from 'src/application/caseFilter';
import { caseSearch } from 'src/application/caseSearch';
import { Product } from 'src/domain/entities/product.entity';
import { ProductController } from '../infrastructure/controllers/product.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Product])],
  providers: [casePalindromo, caseFilter, caseSearch],
  controllers: [ProductController],
})
export class ProductModule {}
