import { ApiProperty } from '@nestjs/swagger';

export class ListDto {
  @ApiProperty()
  readonly id?: number;
  @ApiProperty()
  readonly brand?: string;
}
