import { Entity, ObjectID, ObjectIdColumn, Column } from 'typeorm';

@Entity()
export class Product {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  id: number;

  @Column()
  brand: string;

  @Column()
  description: string;

  @Column()
  image: string;

  @Column()
  price: number;
}
