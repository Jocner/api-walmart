import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Product } from '../domain/entities/product.entity';


@Injectable()
export class casePalindromo {
  constructor(
    @InjectRepository(Product) private productRepo: Repository<Product>,
  ) {}

  async all(param: string): Promise<any> {
    const list = [];
    const product = await this.productRepo.find({ brand: param });

    product.map(async function (objet) {
      const strReversed = objet.brand.split('').reverse().join('');
      const discoun = objet.price * 0.5;

      const article = {
        id: objet.id,
        brand: objet.brand,
        description: objet.description,
        image: objet.image,
        price: objet.price,
        price_discound: discoun,
      };

      const info = strReversed === param ? article : 'no es palindromo';

      list.push(info);
    });

    return list;
  }
}
