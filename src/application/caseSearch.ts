import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Product } from '../domain/entities/product.entity';

@Injectable()
export class caseSearch {
  constructor(
    @InjectRepository(Product) private productRepo: Repository<Product>,
  ) {}

  async detail(param): Promise<any> {


    const product = await this.productRepo.find();

    const info = product.filter((e) => {
  
      if (e.brand.search(param) && e.description.search(param)) {  
        return e;
      }
    });


    return info;
  }
}
