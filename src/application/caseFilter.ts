import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Product } from '../domain/entities/product.entity';

@Injectable()
export class caseFilter {
  constructor(
    @InjectRepository(Product) private productRepo: Repository<Product>,
  ) {}

  async detail(param: string): Promise<any> {
    const id = Number(param);
    const product = await this.productRepo.find({ id: id });


    if (!product) throw new NotFoundException('data null');

    return product;
  }
}
