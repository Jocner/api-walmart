import {
  Controller,
  Get,
  HttpStatus,
  NotFoundException,
  Param,
  Res,
} from '@nestjs/common';
import { casePalindromo } from '../../application/casePalindromo';
import { caseFilter } from '../../application/caseFilter';
import { caseSearch } from '../../application/caseSearch';

@Controller('product')
export class ProductController {
  constructor(
    private casePro: casePalindromo,
    private caseFil: caseFilter,
    private caseSe: caseSearch,
  ) {}

  @Get('/:param')
  async get(@Res() res, @Param('param') param: string): Promise<any> {
    const strReversed = await param.split('').reverse().join('');
    const palindromo = await this.casePro.all(param);
    const datail = await this.caseFil.detail(param);
    const search = await this.caseSe.detail(param);

    let re;

    if (Number(param)) {
      re = datail;
    }
    else if (palindromo.length === 0) {
      re = search;
    }

    else if (strReversed) {
      re = palindromo;
    }



    return res.status(HttpStatus.OK).json(re);
  }
}
