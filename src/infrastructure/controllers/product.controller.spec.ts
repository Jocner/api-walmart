import { Test, TestingModule } from '@nestjs/testing';
import { ProductController } from './product.controller';
import { ProductModule } from '../../product/product.module';
import { casePalindromo } from '../../application/casePalindromo';
import { caseFilter } from '../../application/caseFilter';
import { caseSearch } from '../../application/caseSearch';
import { Product } from '../../domain/entities/product.entity';
import { ModuleTokenFactory } from '@nestjs/core/injector/module-token-factory';

describe('ProductController', () => {
  let appController: ProductController;


  it('should be defined', () => {
    expect(appController).toBeUndefined();
  });

});
