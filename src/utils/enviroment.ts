import * as dotenv from 'dotenv';

dotenv.config();

export const env_vars = {
  PORT: process.env.PORT,
  CONNECTSTRING: process.env.CONNECTSTRING,
};
